# Security

All our servers are fully encrypted using 128bit LUKS2 full disk encryption. If anyone should steal or confiscate our servers, they will not be able to gather any information on the data inside.

Our servers are located in Hamburg / Europe and run on a Proxmox data center. Our servers are fully covered by the GDPR and do not transfer any data to thirds. Learn more in our [privacy policy](privacy.md).

# Open source

Aside of encryption, all software we run on our server is open source. That means anyone can check whether there are security issues, backdoors or any other problems. Learn more on open source in the following video by [Public money — public code](https://publiccode.eu):

<video controls="controls" poster="https://publiccode.eu/img/poster.jpg" crossorigin="crossorigin" width="100%">
    <source src="https://download.fsfe.org/videos/pmpc/pmpc_desktop.mp4" type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;" media="screen and (min-device-width:1000px)">
    <source src="https://download.fsfe.org/videos/pmpc/pmpc_desktop.webm" type="video/webm; codecs=&quot;vp9, opus&quot;" media="screen and (min-device-width:1000px)">
    <source src="https://download.fsfe.org/videos/pmpc/pmpc_mobile.mp4" type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;" media="screen and (max-device-width:999px)">
    <source src="https://download.fsfe.org/videos/pmpc/pmpc_mobile.webm" type="video/webm; codecs=&quot;vp9, opus&quot;" media="screen and (max-device-width:999px)">
    
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_en.vtt" kind="subtitles" srclang="en" label="English">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_de.vtt" kind="subtitles" srclang="de" label="Deutsch">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_es.vtt" kind="subtitles" srclang="es" label="Español">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_eo.vtt" kind="subtitles" srclang="eo" label="Esperanto">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_fr.vtt" kind="subtitles" srclang="fr" label="Français">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_hr.vtt" kind="subtitles" srclang="hr" label="Hrvatski">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_hu.vtt" kind="subtitles" srclang="hu" label="Magyar">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_it.vtt" kind="subtitles" srclang="it" label="Italiano">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_nl.vtt" kind="subtitles" srclang="nl" label="Nederlands">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_nb.vtt" kind="subtitles" srclang="nb" label="Norsk (bokmål)">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_pl.vtt" kind="subtitles" srclang="pl" label="Polski">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_pt.vtt" kind="subtitles" srclang="pt" label="Português">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_ru.vtt" kind="subtitles" srclang="ru" label="Русский">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_sk.vtt" kind="subtitles" srclang="sk" label="Slovenský">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_sq.vtt" kind="subtitles" srclang="sq" label="Shqip">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_sv.vtt" kind="subtitles" srclang="sv" label="Svenska">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_tr.vtt" kind="subtitles" srclang="tr" label="Türkçe">
    <track src="https://publiccode.eu/video-subs/webvtt/pmpc_zh.vtt" kind="subtitles" srclang="zh" label="正體中文（臺灣）">
  </video>